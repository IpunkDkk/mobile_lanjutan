import { Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import React, { Component } from 'react';

const Tombol = ({label , onPress})=> {
  return(
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text>{label}</Text>
    </TouchableOpacity>
  )
}


class Home extends Component {
  render() {
    return (
      <View>
        <Tombol 
        label={"Tugas1"} 
        onPress={() => this.props.navigation.navigate('Tugas1')}
        />
      </View>
    )
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
  },
});
