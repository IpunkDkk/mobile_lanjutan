import { Text, StyleSheet, View , TextInput, TouchableOpacity, Alert } from 'react-native';
import React, { Component } from 'react';

export default class Tugas1 extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          warnaText: 'black',
          inputPertama: '',
          inputKedua: '',
          fullText: '',
        };
      }
      CekAngka = (a, b)=> {
        const Angka = ['1','2','3','4','5','6','7','8','9','0'];
        let angka1;
        let angka2;
        let Kalkulator;
        for (i in Angka){
            if (a === i){
                angka1 = true;
                break;
            }else{
                angka2 = false;
            }
        }
        for (i in Angka){
            if (b === i){
                angka2 =true;
                break;
            }else{
                angka2 =false;
            }
        }

        if (angka1 === true && angka2 === true){
            Kalkulator = true;
            return Kalkulator;
        }else{
            Kalkulator = false;
            return Kalkulator;
        }
      }


      CekOperator = (a) => {
          const operator = ['+' , '/' , '*', '-'];
          let opreturn = '+';
          let ang;
          let op;
          for (ang in a.split('')){
              for (op in operator){
                  if(a[ang] == operator[op]){
                      opreturn = operator[op];
                  }
              }
          }
          return opreturn;
      }

      Result = () => {
          const pertama = this.state.inputPertama
          const kedua = this.state.inputKedua
          if (pertama !== '' && kedua !== ''){
              if (pertama.split('')[0] !== ' ' && kedua.split('')[0] !==' '){
                  let cek = this.CekAngka(pertama.split('')[0], kedua.split('')[0]);
                  let op = this.CekOperator(pertama);
                //   console.log(op);
                  if (cek){
                      let hasil
                      if (op == '+'){
                        hasil = parseFloat(pertama) + parseFloat(kedua) ;
                      }else if(op == '-'){
                        hasil = parseFloat(pertama) - parseFloat(kedua) ;
                      }else if(op == '/'){
                        hasil = parseFloat(pertama) / parseFloat(kedua) ;
                      }else if(op == '*'){
                        hasil = parseFloat(pertama) * parseFloat(kedua) ;
                      }else{
                        hasil = parseFloat(pertama) + parseFloat(kedua) ;
                      }
                      this.setState({fullText:hasil})
                  }
                  else{
                    let hasil = pertama + " " + kedua
                    this.setState({fullText:hasil})
                  }
              }else{
                  Alert.alert("Spasi" ,"Tidak Boleh Diawali Spasi");
              }
          }
          else{
              Alert.alert("Kosong","Tidak Boleh Ada Yang Kosong");
          }
      }
    
  render() {
    return (
      <View style={styles.continer}>
          <View>
            <Text>Selamat Datang Di Aplikasi Saya</Text>
            <Text>Jika Ingin Menggunaka Kalkulator silahkan masukan angka</Text>
            <Text>Jika Ingin Ingin Memasukan operator silhkan masukan di input 1 di akhir</Text>
            <Text>Jika Tidak Memasukan Operator Maka Dinilai +  </Text>
            <Text>Jika Memasukan Text Maka Hanya Digabungkan</Text>
          </View>
          <TextInput style={styles.input} onChangeText={inputText=>this.setState({inputPertama : inputText})}/>
          <TextInput style={styles.input} onChangeText={inputText=>this.setState({inputKedua : inputText})}/>
          <TouchableOpacity style={styles.btn} onPress={() => this.Result()}>
              <Text>Result</Text>
          </TouchableOpacity>
          <Text style={{marginTop:5}}>Result : {this.state.fullText}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    continer : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn: {
        backgroundColor: 'red',
        borderRadius: 4,
        padding: 5,
        marginTop: 10
      },    
    input: {
        backgroundColor: 'lightgrey',
        margin: 2,
        width: '100%',
        paddingHorizontal: 10,
        marginBottom: 10
      },
    
});