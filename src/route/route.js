import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Home, Tugas1 } from '../pages';

const Stack = createNativeStackNavigator();

function Route() {
  return (
    <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Tugas1" component={Tugas1} />
    </Stack.Navigator>
  );
}

export default Route;